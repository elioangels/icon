# Installing icon

- [Prerequisites](./prerequisites)

## Contributing

GIT is a fine way to install **icon**.

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/icon.git
```
