# icon

<figure>
  <img src="star.png" alt="">
</figure>

> Put a smiley on your face, **the elioWay**

![notstarted](//docs.elioway/./elioangels/icon/notstarted/favicon.png "notstarted")

**icon** is a resource bank for **elioWay** project icon.

At some point in the future we should create an **elioAngel** "iconpack builder" here, which does the following:

- Builds 72point elio star icon of specified size from a base image.
- Builds 24point elio star icon of specified size from a base image.
- Builds apple-touch-icon automatically.
- Combines svg icon into stylesheets, like icomoon does.
- Knit **icon** into the **elioSin** framework.

<div><a href="installing.html">
  <button>Installing</button>
</a>
  <a href="quickstart.html">
  <button>Quickstart</button>
</a></div>
