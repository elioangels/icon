# Quickstart art

- [Prerequisites](./prerequisites)

## Creating `ico` format file from a `png`

```
icotool -c favicon.png -o favicon.ico
```

## Guide

![](static/elio72pt.svg)

72pt elio star with black background and a white halo.

![](static/elio24pt.svg)

24pt elio star with black background and a white halo. Useful for smaller icon

![](static/elio12pt.svg)

12pt elio star with black background. Useful for smaller favicons
