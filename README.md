![](https://elioway.gitlab.io/elioangels/icon/elio-icon-logo.png)

> Put a smiley on your face **the elioWay**

# icon ![notstarted](/artwork/icon/notstarted/favicon.png "notstarted")

Web icon and packs done the elioWay.

- [icon Documentation](https://elioway.gitlab.io/icon)

# Installing

- [Installing icon](https://elioway.gitlab.io/icon/installing.html)

# Nutshell

- [icon Quickstart](https://elioway.gitlab.io/elioangels/icon/quickstart.html)
- [icon Credits](https://elioway.gitlab.io/elioangels/icon/credits.html)

![](https://elioway.gitlab.io/elioangels/icon/apple-touch-icon.png)

# License

[MIT](LICENSE) [Tim Bushell](mailto:tcbushell@gmail.com)
